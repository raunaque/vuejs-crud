import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import TutorialCreate from '@/components/TutorialCreate'
import TutorialEdit from '@/components/TutorialEdit'
import TutorialDelete from '@/components/TutorialDelete'

Vue.use(Router)

export default new Router({
  mode : 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/add-tutorial',
      name: 'TutorialCreate',
      component: TutorialCreate
    },
    {
      path: '/edit-tutorial/:id',
      name: 'TutorialEdit',
      component: TutorialEdit
    },
    {
      path: '/delete-tutorial/:id',
      name: 'TutorialDelete',
      component: TutorialDelete
    }
  ]
})
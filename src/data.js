let tutorials =  [
  {
    name: 'Java Script',
    description: 'Very powerful language',
    price: 8000,
    id: 1
  },
  {
    name: 'Vue Js',
    description: 'A Javascript library/framework based on components',
    price: 10000,
    id: 2
  },
  {
    name: 'Angular Js',
    description: 'A Javascript library/framework based on components',
    price: 15000,
    id: 3
  }
]
export default tutorials;